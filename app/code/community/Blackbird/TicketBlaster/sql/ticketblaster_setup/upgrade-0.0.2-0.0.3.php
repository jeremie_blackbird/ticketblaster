<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    'event_reference',
    array(
        'type'                    => 'int',
        'input'                   => 'select',
        'label'                   => 'Event of this ticket',
        'apply_to'                => 'tickets',
        'source'                  => 'ticketblaster/attribute_source_event',
        'visible'                 => true,
        'user_defined'            => true,
        'searchable'              => false,
        'filterable'              => false,
        'comparable'              => false,
        'required'                => true,
        'used_in_product_listing' => true,
        'visible_on_front'        => true,
        'global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    )
);

$attributeId = $installer->getAttributeId(
        'catalog_product',
        'event_reference'
);

$defaultSetId = $installer->getAttributeSetId('catalog_product', 'default');

$installer->addAttributeGroup(
        'catalog_product',
        $defaultSetId,
        'TicketBlaster'
);

$groupId = $installer->getAttributeGroup(
        'catalog_product',
        $defaultSetId,
        'TicketBlaster',
        'attribute_group_id'
);

if ($attributeId > 0) {
    $installer->addAttributeToSet(
            'catalog_product',
            $defaultSetId,
            $groupId,
            $attributeId
    );
}
 
$installer->endSetup();