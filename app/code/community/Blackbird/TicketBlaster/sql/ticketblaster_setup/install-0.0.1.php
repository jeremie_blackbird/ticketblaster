<?php

$installer = $this;
$installer->startSetup();

/**
 * Create table 'ticketblaster/event'
*/
$table = $installer->getConnection()
->newTable($installer->getTable('ticketblaster/event'))
->addColumn('event_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
), 'Event ID')
->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
), 'Event Name')
->addColumn('venue', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
        'nullable'  => false,
), 'Venue Name')
->addColumn('datetime', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Event Datetime')
->addColumn('creation_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Event Creation Time')
->addColumn('update_time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
), 'Event Modification Time')
->setComment('TicketBlatser Event Table');
$installer->getConnection()->createTable($table);

$installer->endSetup();