<?php

class Blackbird_TicketBlaster_Model_Product_SalableTest extends PHPUnit_Framework_TestCase
{
    protected $event;
    protected $product;
    protected $dateFormatIso;
    CONST PRODUCT_ID = 907;
    
    public function setUp()
    {
        $this->product = Mage::getModel('catalog/product')->getCollection()
                                    ->addAttributeToFilter('entity_id', $this::PRODUCT_ID)
                                    ->addAttributeToSelect('event_reference')
                                    ->getFirstItem();
        
        $this->event = Mage::getModel('ticketblaster/event')
                                    ->getCollection()
                                    ->addFieldToFilter('event_id', $this->product->getEventReference())
                                    ->getFirstItem();
        
        $this->dateFormatIso = Mage::app()->getLocale()->getDateFormat(
                Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
    }
    
    public function testProductIsSalable()
    {
        // We want to sell our tickets before the begining of the event. 
        // So we try to know it for the day before.
        $tomorrowDateTime = Mage::app()->getLocale()->date()->add(1, Zend_Date::DAY);
        $this->event->setDatetime($tomorrowDateTime->toString($dateFormatIso));
        $this->event->save();
        
        $this->assertTrue($this->product->isSalable());
    }
}
