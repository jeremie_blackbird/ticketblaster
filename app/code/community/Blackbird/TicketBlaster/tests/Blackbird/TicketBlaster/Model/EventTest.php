<?php

class Blackbird_TicketBlaster_Model_EventTest extends PHPUnit_Framework_TestCase
{
    protected $event;
    
    public function setUp()
    {
        $this->event = Mage::getModel('ticketblaster/event')
                                    ->getCollection()->getFirstItem();
    }
       
    public function testEvent()
    {
        $this->assertInstanceOf('Blackbird_TicketBlaster_Model_Event', $this->event);
    }
    
    public function testVenueName()
    {
        $this->assertNotNull($this->event->getVenue());
    }
    
    /*public function testFailure()
    {
        $this->assertSame($this->event->getVenue(), $this->event->getName());
    }*/
}
