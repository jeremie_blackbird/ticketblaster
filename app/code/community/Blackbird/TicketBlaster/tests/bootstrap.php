<?php

// Set the Magento root folder. You can use both relative and complete path
define('MAGENTO_ROOT', "/home/web/magento-1-9-1-0/www");
//define('MAGENTO_ROOT', "../../../../../..");

// Require Mage.php allow us to access to the Mage final class
require_once MAGENTO_ROOT . '/app/Mage.php';

// With this line, we initiate the Magento instance
Mage::app('default');
