<?php

class Blackbird_TicketBlaster_Model_Attribute_Source_Event extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            
            $eventsCollection = Mage::getModel('ticketblaster/event')->getCollection();
            $this->_options = array();
            
            foreach($eventsCollection as $event){
                $option = array(
                        'label' => $event->getName(),
                        'value' => $event->getId(),
                );
                $this->_options[] = $option;
            }
        }
        return $this->_options;
    }
    
    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}
