<?php

class Blackbird_TicketBlaster_Model_Product_Type extends Mage_Catalog_Model_Product_Type_Virtual
{
    // We create a class property to limit time & performance consuming
    protected $_event;
    
    public function isSalable($product = null)
    {
        $_isSalable = parent::isSalable($product);
        return ($_isSalable && $this->isEventSalable($product));
    }
    
    public function isEventSalable($product)
    {
        $event = $this->getEvent($product);
        
        $todayDateTime = Mage::app()->getLocale()->date();
        $eventDate = Mage::app()->getLocale()->date(strtotime($event->getDatetime()));

        return $todayDateTime->isEarlier($eventDate);
    }
    
    public function getEvent($product)
    {
        if(is_null($this->_event)){
            $this->_event = Mage::getModel('ticketblaster/event')->getCollection()
                                ->addFieldToFilter('event_id', $this->getProduct($product)->getEventReference())
                                ->getFirstItem();
        }
        
        return $this->_event;
    }
}
