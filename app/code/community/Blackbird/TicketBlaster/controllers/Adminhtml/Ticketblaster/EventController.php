<?php

class Blackbird_TicketBlaster_Adminhtml_Ticketblaster_EventController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu('blackbird');
        return $this;
    }
    
    public function indexAction()
    {
        $this->loadLayout();
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('ticketblaster/adminhtml_event'));
        $this->renderLayout();
    }
    
    public function editAction()
    {
        $eventId     = $this->getRequest()->getParam('event_id');
        $eventModel  = Mage::getModel('ticketblaster/event')->load($eventId);
    
        if ($eventModel->getId() || $eventId == 0) {
    
            Mage::register('event_data', $eventModel);
    
            $this->loadLayout();
            $this->_setActiveMenu('ticketblaster');
             
            $this->_addContent($this->getLayout()->createBlock('ticketblaster/adminhtml_event_edit'))
            ->_addLeft($this->getLayout()->createBlock('ticketblaster/adminhtml_event_edit_tabs'));
             
            $this->renderLayout();
        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ticketblaster')->__('Event does not exist'));
            $this->_redirect('*/*/');
        }
    }
     
    public function newAction()
    {
        $this->_forward('edit');
    }
     
    public function saveAction()
    {
        if ($data = $this->getRequest()->getParams()) {
            try {
                //init model and set data
                $eventModel = Mage::getModel('ticketblaster/event');
    
                if ($id = $this->getRequest()->getParam('event_id')) {
                    $eventModel->load($id);
                }
                 
                $eventModel->setData($data);
    
                if( $this->getRequest()->getParam('event_id') ) {
                    $eventModel->setUpdateTime(Mage::getSingleton('core/date')->gmtDate());
                } else{
                    $eventModel->setCreatedTime(Mage::getSingleton('core/date')->gmtDate());
                }
    
                $eventModel->save();
                 
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Event was successfully saved'));
                Mage::getSingleton('adminhtml/session')->setEventData(false);
    
                // check if 'Save and Continue'
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('event_id' => $eventModel->getEventId()));
                    return;
                }
    
                $this->_redirect('*/*/');
                return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setEventData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('event_id' => $this->getRequest()->getParam('event_id')));
                return;
            }
        }
        $this->_redirect('*/*/');
    }
     
    public function deleteAction()
    {
        if( $this->getRequest()->getParam('event_id') > 0 ) {
            try {
                $eventModel = Mage::getModel('ticketblaster/event');
                 
                $eventModel->setId($this->getRequest()->getParam('event_id'))
                ->delete();
                 
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Event was successfully deleted'));
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('event_id' => $this->getRequest()->getParam('event_id')));
            }
        }
        $this->_redirect('*/*/');
    }
    
}