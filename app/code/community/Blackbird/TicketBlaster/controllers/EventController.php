<?php
class Blackbird_TicketBlaster_EventController extends Mage_Core_Controller_Front_Action
{
    public function listAction(){
        // Some actions
        
        Varien_Profiler::start("Blackbird");
        
        $this->loadLayout();
        $this->renderLayout();
    }
}
