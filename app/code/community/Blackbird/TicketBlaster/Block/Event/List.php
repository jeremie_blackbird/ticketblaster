<?php

class Blackbird_TicketBlaster_Block_Event_List extends Mage_Core_Block_Template
{
    public function getEvents()
    {
       $events = Mage::getModel('ticketblaster/event')->getCollection();
       return $events;
    }
}
