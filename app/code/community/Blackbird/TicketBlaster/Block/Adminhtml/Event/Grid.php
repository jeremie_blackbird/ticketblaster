<?php

class Blackbird_TicketBlaster_Block_Adminhtml_Event_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('eventGrid');
        // This is the primary key of the database
        $this->setDefaultSort('event_id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('ticketblaster/event')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
 
    protected function _prepareColumns()
    {
        $this->addColumn('event_id', array(
            'header'    => Mage::helper('ticketblaster')->__('ID'),
            'align'     => 'center',
            'width'     => '20px',
            'index'     => 'event_id',
        ));
 
        $this->addColumn('name', array(
            'header'    => Mage::helper('ticketblaster')->__('Name'),
            'index'     => 'name',
        ));
        
        $this->addColumn('venue', array(
            'header'    => Mage::helper('ticketblaster')->__('Venue'),
            'align'     => 'left',
            'width'     => '100px',
            'index'     => 'venue',
        ));
        
        $this->addColumn('datetime', array(
                'header'    => Mage::helper('ticketblaster')->__('Event date'),
                'align'     => 'left',
                'width'     => '100px',
                'index'     => 'datetime',
                'type'      => 'date',
        ));
 
        $this->addColumn('created_time', array(
            'header'    => Mage::helper('ticketblaster')->__('Creation Time'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'created_time',
        ));
 
        $this->addColumn('update_time', array(
            'header'    => Mage::helper('ticketblaster')->__('Update Time'),
            'align'     => 'left',
            'width'     => '120px',
            'type'      => 'date',
            'default'   => '--',
            'index'     => 'update_time',
        ));
 
        return parent::_prepareColumns();
    }
 
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('event_id' => $row->getId()));
    }
}