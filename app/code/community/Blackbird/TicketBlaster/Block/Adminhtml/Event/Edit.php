<?php

class Blackbird_TicketBlaster_Block_Adminhtml_Event_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'event_id';
        $this->_blockGroup = 'ticketblaster';
        $this->_controller = 'adminhtml_event';
        parent::__construct();
 
        $this->_updateButton('save', 'label', Mage::helper('ticketblaster')->__('Save'));
        $this->_updateButton('delete', 'label', Mage::helper('ticketblaster')->__('Delete'));
        
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }
 
    public function getHeaderText()
    {
        if( Mage::registry('event_data') && Mage::registry('event_data')->getId() ) {
            return Mage::helper('ticketblaster')->__("Edit Question");
        } else {
            return Mage::helper('ticketblaster')->__('Add Question');
        }
    }
}