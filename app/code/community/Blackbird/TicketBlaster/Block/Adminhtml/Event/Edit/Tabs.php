<?php

class Blackbird_TicketBlaster_Block_Adminhtml_Event_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('event_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ticketblaster')->__('Manage events'));
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label'     => Mage::helper('ticketblaster')->__('Event'),
            'title'     => Mage::helper('ticketblaster')->__('Event'),
            'content'   => $this->getLayout()->createBlock('ticketblaster/adminhtml_event_edit_tab_form')->toHtml(),
        ));
        return parent::_beforeToHtml();
    }
}