<?php

class Blackbird_TicketBlaster_Block_Adminhtml_Event_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('event_form', array('legend'=>Mage::helper('ticketblaster')->__('Event')));
       
        $fieldset->addField('name', 'text', array(
                'name'      => 'name',
                'label'     => Mage::helper('ticketblaster')->__('Name of the event'),
                'class'     => 'required-entry',
                'required'  => true,
        ));
        
        $fieldset->addField('venue', 'text', array(
                'name'      => 'venue',
                'label'     => Mage::helper('ticketblaster')->__('Venue'),
                'class'     => 'required-entry',
                'required'  => true,
                'note'      => 'Please note as precisely as possible',
        ));
        
        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
                Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        
        $fieldset->addField('datetime', 'date', array(
                'name'      => 'datetime',
                'label'     => Mage::helper('ticketblaster')->__('Date of the event'),
                'format'    => $dateFormatIso,
                'image'     => $this->getSkinUrl('images/grid-cal.gif'),
                'time' => true
        ));

        if ( Mage::getSingleton('adminhtml/session')->getEventData() )
        {
            $form->setValues(Mage::getSingleton('adminhtml/session')->getEventData());
            Mage::getSingleton('adminhtml/session')->setEventData(null);
        } elseif ( Mage::registry('event_data') ) {
            $form->setValues(Mage::registry('event_data')->getData());
        }
        return parent::_prepareForm();
    }
}