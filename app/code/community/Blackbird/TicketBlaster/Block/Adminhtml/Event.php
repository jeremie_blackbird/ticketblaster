<?php

class Blackbird_TicketBlaster_Block_Adminhtml_Event extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct(){
		$this->_controller = 'adminhtml_event';
        $this->_blockGroup = 'ticketblaster';
        $this->_headerText = Mage::helper('ticketblaster')->__('Events Manager');
        $this->_addButtonLabel = Mage::helper('ticketblaster')->__('Add Event');
        parent::__construct();
	}
}