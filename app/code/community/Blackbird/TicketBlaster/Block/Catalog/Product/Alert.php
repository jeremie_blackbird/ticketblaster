<?php

class Blackbird_TicketBlaster_Block_Catalog_Product_Alert extends Mage_Catalog_Block_Product_Abstract
{
    public function displayTimedCheckoutMessage()
    {
        return !$this->getProduct()->isSalable();
    }
}
