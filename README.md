# README #

# **TicketBlaster** repository - **Magento Extensions Development** #

This code is the **study Magento extension** I wrote for the "Magento Extensions Development" book (Packt Publishing)

*It is primary destined to the readers of my book*, but this extension is fully functional for all Magento (>v1.6)

You can freely download or fork this repository, test the code and make pull requests.

Jérémie